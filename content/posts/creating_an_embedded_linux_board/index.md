---
title: "Creating an embedded linux board"
date: 2023-06-06T13:28:55+02:00
draft: true
toc: false
images:
tags:
  - untagged
---

In this post, an embedded linux board will be created. It will be done from scratch. Both the hardware and the software will be explained.

# The hardware involved

The embedded board will be based on a [cheap](https://hackaday.com/2018/09/17/a-1-linux-capable-hand-solderable-processor/) Allwinner A13 chip.

# Previous knowledge

This post assumes some knowledge about buildroot and embedded linux.

# Differences with existing embedded linux boards

- There is no probuilt device tree (`.dtb`) file, since the board is newly created and has special hardware.
- The Raspberry Pi linux kernel source is replaced by the sunxi linux source.
- Crosstool-NG is absolutely required, since the cortex A8 cpu has a special instruction set.
- The allwinner A13 is 32 bit.

# Creating the hardware

TODO!

# Creating a toolchain

To get a working cross-compiling toolchain for this project, Crosstool-NG has to be used. The normal cross-compiling toolchain from the apt-repo's doesn't include a proper assembler for the cortex A8 cpu.

## Building Crosstool-NG

To build Crosstool-NG, it is downloaded from the github master branch. Do look at the commits and see which one builds without errors.

Building Crosstool-NG goes as follows for this project:

```bash
git clone https://github.com/crosstool-ng/crosstool-ng
cd crosstool-ng
./bootstrap
./configure --enable-local
make -j`nproc`
```

## Building a cross-compiling toolchain

Within the `crosstool-ng` directory, do the following:

```bash
./ct-ng list-samples
./ct-ng arm-cortex8-linux-gnueabi
./ct-ng menuconfig # set linux version to 5.8.15 (set it close to the linux version)
./ct-ng build
```

# Building the linux kernel

## The sunxi linux source

Just like the Raspberry Pi, allwinner chips have [a modified linux version](https://github.com/linux-sunxi/linux-sunxi) that's tailored to the hardware. In this project, this modified linux version will be used. The mainline linux kernel may work, but stuff like hardware acceleration will probably be missing from it.

## Downloading

The sunxi linux kernel will be downloaded as a github release file (`.tar`). At the time of writing, the latest linux release was `v5.8.0`. Any release candidates are considered too unstable in this project and will not be used.

## Setting up shell variables

ARCH=arm
CROSS_COMPILE=/home/$USER/x-tools/????????????

## Creating a default .config file

Many outdated webpages will say that `make sun5i_defconfig` will create a default configuration. This is no longer correct. The following command can be used instead:

```bash
make sunxi_defconfig
```

This will enable support for the allwinner A10, A13 (this project's SoC), ????????????????

To turn off support for all the other chips, execute

```bash
make menuconfig
```

and visit ????????????????? to disable support for all other allwinner SoC's.

## Building the linux kernel

Then, the linux kernel can be built using `make`:

```bash
make -j`nproc`
```

# Creating a device tree for the custom board

Normally, when an embedded linux board is bought, the vendor already created a device tree (`.dtb` file) for all the hardware on the board. Because this project uses custom hardware, a custom device tree has to be created. This is quite a process in itself.

TODO

# Compiling U-boot for the board

Normally, U-boot already knows about existing device trees. When the board is started, U-boot will do it's thing. In this project, U-boot has to receive the custom-built device tree (`.dtb` file) before it can interact with all the hardware.

## Setting up the shell again

ARCH=arm
CROSS_COMPILE=/home/$USER/x-tools/????????????

## Setting up device tree magic

## Building U-boot

At the end, a file called `u-boot.bin` should exist

## Creating a boot script

U-boot needs some sort of boot configuration before it starts to do something.

?????????????