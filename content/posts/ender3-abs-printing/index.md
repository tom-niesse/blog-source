---
title: "Printing ABS using an Ender 3 (v1), without an enclosure"
date: 2023-04-09T13:28:55+02:00
draft: false
toc: false
images:
tags:
  - untagged
---

# Introduction

When looking for a guide on how to print ABS using a cheap printer, people usually say "it's impossible", "it requires a printer enclosure" or "you just have to remove the fans".
I disagree with all those statements. In [this youtube video](https://www.youtube.com/watch?v=wjSpVN19x7A), someone put duct tape on their Ender 3 and was able to print ABS just fine.
And if someone else can do it, it must be possible.

This page documents the adventure of ABS printing using a 3D printer without an enclosure.

# Airflow and duct tape

The main issue with ABS printing on an Ender 3 is that the cooling fan (not the print cooling fan) blows air down, onto the print.
If the air is directed away from the print, ABS will print just fine.

After applying duct tape to my own printer, I got similar results: it produced a good print!
However, it also melted all the duct tape. This made a big mess.

[![The duct tape solution](images/ducttape_solution.jpg)](images/ducttape_solution.jpg)

# Adding a new airflow cover to the printer

Because duct tape was not a long term solution, I created a new part for the Ender 3 v1. The stl file is [hosted on thingiverse](https://www.thingiverse.com/thing:5631450).

The designed cover was first printed in PLA. This went as well as one would expect: it melted pretty quickly during printing.
It held up just long enough to print another cover (made out of ABS).

[![The PLA and ABS airflow covers](images/pla_vs_abs_cover.jpg)](images/pla_vs_abs_cover.jpg)

The ABS cover (on the right) has been attached to my printer ever since, and is doing just fine! It doesn't deform like the PLA cover did.

# Differences between PLA and ABS

ABS is a little different when it comes to printing. Here's my experience with it so far:

#### Pros

- As long as the airflow cover is on, the ambient temperature can be as low as 17°C. Prints will still come out just fine
- As long as the print bed is hot (110°C, the maximum bed temperature for the Ender 3 v1), the ABS sticks to the bed very well.
  Just like with small PLA prints though, a brim may be required.

#### Cons

- The "small perimeter speed" and "overhang speed" need some tuning before prints start to look well.
- When printing very fast, ABS doesn't always hold it's shape.
  Sometimes, walls will "move inwards". This then creates a ")("-shape instead of a "||"-shape.
  The only solution I have found so far is to print more slowly.
- When printing large ABS prints, warping and delamination may occur.

# Printing a benchy using ABS

To test how well the airflow cover would work, I printed a benchy. It came out looking very well, but the roof was a little weak due to slight delamination.
This probably had to do with cooling, since the rest of the boat was just fine. The below image shows the Ender 3 printing a benchy out of ABS.
No enclosure was used and the ambient temperature was around 17°C that day.

[![Printing a benchy without a brim](images/benchy.jpg)](images/benchy.jpg)

# Summary

Printing ABS is possible using an Ender 3 v1. It's not perfect, but still very doable. The delamination is still an issue, but I think it can be fixed.
