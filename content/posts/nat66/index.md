---
title: "Daisy chained routers and IPv6 (NAT66)"
date: 2023-06-04T11:28:55+02:00
draft: false
toc: false
images:
tags:
  - untagged
---

For a very specific problem, I needed to daisy chain some routers. This is easily done with IPv4, since it has has NAT. But what about IPv6?

# The issue

I recently bought a cheap second hand router. I did this to split the incoming WAN-connection into two LAN-connections. One LAN-connection goes to a separate network with a specific purpose. The other LAN-connection serves as a WAN for another router. The second router is not mounted near a fuse box and can serve much better wifi to the entire house.

[![Topology block diagram](images/topology_block_diagram.png)](images/topology_block_diagram.png)

Using this topology, IPv4 worked almost automatically. However, IPv6 did not. For router 2, this does not matter that much. For router 3 however, it does matter (I want IPv6 when connecting to wifi). Many answers online involve calling the ISP and asking for another IPv6 prefix. I wasn't going to do this.

# The solution

Turns out, IPv6 masquerading exists. And it works just like IPv4's NAT. If both routers could just NAT all IPv6 traffic, I consider my problem solved!

## Router 1 settings

In router 1, I set a LAN IPv6 as follows:

[![Router 1 settings](images/router1_ipv6_lan_settings.png)](images/router1_ipv6_lan_settings.png)

## Router 3

In router 3, an OpenWRT router, I changed a few settings (as per the [OpenWRT wiki](https://openwrt.org/docs/guide-user/network/ipv6/ipv6.nat6)):

In Network->Firewall->Zones->WAN->Advanced settings, I enabled IPv6 masquerading. This enables NAT6 and causes all IPv6 traffic to be translated, just like with IPv4.

[![Router 3 firewall setting](images/router3_firewall_setting.png)](images/router3_firewall_setting.png)

In Network-> Interfaces->wan6->Advanced Settings I disabled IPv6 source routing:

[![Router 3 source routing setting](images/router3_ipv6_source_routing.png)](images/router3_ipv6_source_routing.png)

Then, some extra settings were modified to make DHCPv6 work.

In Network->Interfaces->lan->DHCP Server->IPv6 Settings, I make sure that router3 advertises itself as a DHCPv6 server:

[![Router 3 DHCP IPv6 settings](images/router3_dhcp_ipv6_settings.png)](images/router3_dhcp_ipv6_settings.png)

In Network->Interfaces->lan->DHCP Server->IPv6 Settings, I make sure that the Router Advertisement (RA) settings are correctly set up. I enabled SLAAC, made sure that the `M`-flag is enabled in the RA-flags and that default router is on `on available prefix`. This way, router 3 will always hand out IPv6 addresses, even if there is no connection to router 1. The settings look as follows:

[![Router 3 IPv6 RA settings](images/router3_IPv6_RA_settings.png)](images/router3_IPv6_RA_settings.png)

Finally, to make sure router 3 routes all IPv6 traffic to router 1, I set up a static IPv6 route. This is done in Network->Routing->Static IPv6 Routes:

[![Router 3 static IPv6 route](images/router3_static_ipv6_route.png)](images/router3_static_ipv6_route.png)

When configured, the list of static routes looks as follows:

[![Router 3 static IPv6 routes](images/router3_static_ipv6_routes.png)](images/router3_static_ipv6_routes.png)

## Debugging

When pinging websites using IPv6, I got a connection. However, if it doesn't work, `traceroute` can help figure out where the problem is:

- No DNS? In router 3, manually set a DNS address in DHCPv6 settings and see if things work now.
- Transmit failure? Router 3's DHCPv6 settings need some changes.
- Timeout? Router 3 doesn't have a (properly configured) IPv6 static route or router 1 doesn't forward the packets (properly).
