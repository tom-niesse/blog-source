---
title: "Introduction to fusion 360"
date: 2023-04-16T13:28:55+02:00
draft: true
toc: false
images:
tags:
  - untagged
---

# What is Fusion 360?

Autodesk's Fusion 360 is a 3D modeling program. If used non-commercially, it is free of costs. There are already many articles and youtube videos about how to use Fusion 360, but they make the learning curve seem bigger than it is. That is why I made this blogpost. A 3D object from [another post](/posts/ender3-abs-printing/) was created using Fusion 360.

# Creating a basic shape

TODO

# Creating 3D text

TODO

# Creating a screw

TODO

# Creating a fan

TODO

# Conclusion

Autodesk Fusion 360 is not difficult to use, as long as the basic steps and idea's are known to a user. In this blogpost, some example objects were made to show the basics of Fusion 360.